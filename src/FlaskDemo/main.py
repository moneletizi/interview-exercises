from flask import Flask
from flask_restful import Api, Resource, reqparse, abort

app = Flask(__name__)
api = Api(app)

videos = {}
video_model_args = reqparse.RequestParser()
video_model_args.add_argument("name", type=str, help="Name of the video is required", required = True)
video_model_args.add_argument("likes", type=int, help="Likes of the video is required", required = True)
video_model_args.add_argument("views", type=int, help="Views of the video id required", required = True)

def abort_if_video_id_does_not_exists(video_id):
    if video_id not in videos:
        abort(404, message = "Video id does not exists")

def abort_if_video_id_already_exists(video_id):
    if video_id in videos:
        abort(409, message = "Video already exists with that id")


class Video(Resource):
    def get(self, video_id):
        abort_if_video_id_does_not_exists(video_id)
        return videos[video_id]

    def put(self, video_id):
        abort_if_video_id_already_exists(video_id)
        body = video_model_args.parse_args()
        print("body", body)
        videos[video_id] = video_model_args.parse_args()
        return {video_id: body}, 201

    def delete(self, video_id):
        abort_if_video_id_does_not_exists(video_id)
        del videos[video_id]
        return '', 204


api.add_resource(Video, "/video/<string:video_id>")

if __name__ == "__main__":
    app.run(debug=True)
