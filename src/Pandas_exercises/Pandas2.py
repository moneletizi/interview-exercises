"""Exercises from: https://www.kaggle.com/icarofreire/pandas-24-useful-exercises-with-solutions"""
from datetime import datetime

import pandas as pd
import numpy as np

"""Create a pandas series from each of the items below: a list, numpy and a dictionary"""
def ex1():
    ser = pd.Series([1,2,3,4])
    ser2 = pd.Series({'a' : 1, 'b' : 2, 'c' : 3, 'd' : 4})
    ser3 = pd.Series(np.array([1,2,3,4]))
    print(ser)
    print(ser2)
    print(ser3)

"""Combine ser1 and ser2 to form a dataframe."""
def ex2():
    # input
    ser1 = pd.Series(list('abcedfghijklmnopqrstuvwxyz'))
    ser2 = pd.Series(np.arange(26))
    print(pd.DataFrame(ser1,ser2))

    #Solution 2
    print(pd.DataFrame({'letters':ser1, 'nums': ser2}))

"""3. How to get the items of series A not present in series B?"""
def ex3():
    # input
    ser1 = pd.Series([1, 2, 3, 4, 5])
    ser2 = pd.Series([4, 5, 6, 7, 8])
    print("SER 1")
    print(ser1)
    print("SER 2")
    print(ser2)
    print(ser1[-ser1.isin(ser2)])

"""4. How to get the items not common to both series A and series B?"""
def ex4():
    # input
    ser1 = pd.Series([1, 2, 3, 4, 5])
    ser2 = pd.Series([4, 5, 6, 7, 8])

    a_not_in_b = ser1[-ser1.isin(ser2)]
    b_not_in_a = ser2[-ser2.isin(ser1)]
    print(a_not_in_b.append(b_not_in_a))

"""Compute the minimum, 25th percentile, median, 75th, and maximum of ser."""
def ex5():
    # input
    state = np.random.RandomState(100)
    ser = pd.Series(state.normal(10, 5, 25))

    #Sol 1
    print(ser.describe())

    #Sol 2
    min = ser.min()
    max = ser.max()
    print(min)
    print(max)
    print(ser.quantile([0.25,0.5,0.75]))

"""Calculate the frequency counts of each unique value ser."""
def ex6():
    # input
    ser = pd.Series(np.take(list('abcdefgh'), np.random.randint(8, size=30)))
    print(ser)
    print("UNIQUE")
    print(ser.value_counts())

    #Solution 2
    np_array = ser.to_numpy()
    unique_elements, counts = np.unique(np_array, return_counts=True)
    print(pd.DataFrame({'elements':unique_elements, 'freq':counts}))

"""Reshape the series ser into a dataframe with 7 rows and 5 columns"""
def ex7():
    # input
    ser = pd.Series(np.random.randint(1, 10, 35))
    print(ser)

    #Solution
    ser_reshaped = ser.to_numpy().reshape(5,7)
    print("RESHAPED")
    print(pd.DataFrame(ser_reshaped))

"""Find the positions of numbers that are multiples of 3 from ser."""
def ex8():
    # input

    np.random.RandomState(100)
    ser = pd.Series(np.random.randint(1, 5, 10))
    print(ser)

    # Solution 1
    print(np.where(ser % 3 == 0))
    # Solution 2
    print(ser.where(lambda x: x % 3 == 0).dropna())

"""From ser, extract the items at positions in list pos."""
def ex9():
    ser = pd.Series(list('abcdefghijklmnopqrstuvwxyz'))
    pos = [0, 4, 8, 14, 20]

    #Solution
    print(ser.iloc[pos])

"""Stack ser1 and ser2 vertically and horizontally (to form a dataframe)."""
def ex10():
    # input
    ser1 = pd.Series(range(5))
    ser2 = pd.Series(list('abcde'))

    #Solution 1
    print(ser1.append(ser2)) #stack vertically
    print(pd.DataFrame({'ser1':ser1,'ser2':ser2}))

    #Solution 2 (using concat)
    print("------")
    print(pd.concat([ser1, ser2], axis=0))
    print(pd.concat([ser1, ser2], axis=1))


"""Get the positions of items of ser2 in ser1 as a list."""
def ex11():
    # input
    ser1 = pd.Series([10, 9, 6, 5, 3, 1, 12, 8, 13])
    ser2 = pd.Series([1, 3, 10, 13])
    print(np.where(ser1.isin(ser2)))
    print("----")
    #Solution 2
    print(ser1[ser1.isin(ser2).index])

"""Difference of differences between the consequtive numbers of ser."""
def ex12():
    # input
    ser = pd.Series([1, 3, 6, 10, 15, 21, 27, 35])

    # Desired Output
    # [nan, 2.0, 3.0, 4.0, 5.0, 6.0, 6.0, 8.0]
    # [nan, nan, 1.0, 1.0, 1.0, 1.0, 0.0, 2.0]
    diff = ser-ser.shift(1)
    diff2 = diff - diff.shift(1)
    print(list(diff))
    print(list(diff2))

"""13. How to convert a series of date-strings to a timeseries?"""
def ex13():
    # input
    ser = pd.Series(['01 Jan 2010', '02-02-2011', '20120303', '2013/04/04', '2014-05-05', '2015-06-06T12:20'])
    print(pd.to_datetime(ser))

    #Solution 2
    from dateutil.parser import parse
    ser.map(lambda x: parse(x))


"""From ser, extract words that contain at least 2 vowels."""
def ex14():
    # input
    ser = pd.Series(['Apple', 'Orange', 'Plan', 'Python', 'Money'])

    #Solution 1
    def count_vowels(any_string):
        return sum([1 if str(letter).lower() in list("aeiou") else 0 for letter in list(any_string)])
    at_least_2_vowels = ser.apply(lambda x: count_vowels(x) >= 2)
    print(ser[at_least_2_vowels])

    print("-----")

    #Solution 2
    from collections import Counter
    mask = ser.map(lambda x: sum([Counter(x.lower()).get(i, 0) for i in list('aeiou')]) >= 2)
    print(ser[mask])

"""Replace the spaces in my_str with the least frequent character."""
def ex15():
    # input
    my_str = 'dbc deb abed ggade'

    #Solution 1
    ser = pd.Series(list(my_str))
    print(ser.value_counts())
    least_freq = str(ser.value_counts()[[-1]].index[0])
    print(least_freq)
    my_str = my_str.replace(' ', least_freq)
    print(my_str)


if __name__ == '__main__':
    ex15()