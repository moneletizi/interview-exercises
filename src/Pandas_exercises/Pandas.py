import pandas as pd
import numpy as np
import math

"""Exercises from: https://www.machinelearningplus.com/python/101-pandas-exercises-python/"""

"""Invented by me. Implement st.dev and variance from a known distribution"""
def ex1():
    distribution = np.array([1,3,4,5,6,7,4,5,6])
    variance = np.mean((distribution - np.mean(distribution)) ** 2)
    print("Variance:", variance)
    std_dev = math.sqrt(variance)
    print("Standard deviation:", std_dev)
    print(distribution.std())
    assert variance == distribution.var()
    #assert std_dev == distribution.std()


"""Create a pandas series from each of the items below: a list, numpy and a dictionary"""
def ex2():
    mylist = list('abcedfghijklmnopqrstuvwxyz')
    myarr = np.arange(26)
    mydict = dict(zip(mylist, myarr))
    series = pd.Series(mylist)
    series2 = pd.Series(myarr)
    series3 = pd.Series(mydict)
    print(series)
    print(series2)
    print(series3)
    return series, series2, series3

"""Convert the series ser into a dataframe with its index as another column on the dataframe."""
def ex3():
    series1, series2, series3 = ex2()
    df = pd.DataFrame(series3)
    df = df.reset_index()
    print(df.head())

"""Combine ser1 and ser2 to form a dataframe."""
def ex4():
    series1, series2, series3 = ex2()
    df = pd.DataFrame(data=[series1, series2], axis = 1)
    # df = pd.DataFrame({'col1': ser1, 'col2': ser2})
    print(df.head())

"""From ser1 remove items present in ser2."""
def ex6():
    ser1 = pd.Series([1, 2, 3, 4, 5])
    ser2 = pd.Series([4, 5, 6, 7, 8])
    print(ser1[-ser1.isin(ser2)])

"""Get all items of ser1 and ser2 not common to both."""
def ex7():
    ser1 = pd.Series([1, 2, 3, 4, 5])
    ser2 = pd.Series([4, 5, 6, 7, 8])
    intersection = set(ser1) & set(ser2)
    print(f'Intersection: {intersection}')
    print(pd.concat(objs=[ser1[-ser1.isin(intersection)], ser2[-ser2.isin(intersection)]]))

"""Compute the minimum, 25th percentile, median, 75th, and maximum of ser."""
def ex8():
    ser = pd.Series(np.random.normal(10, 5, 25))
    print(f'min {ser.min()}')
    print(f'mean {ser.mean()}')
    print(f'max {ser.max()}')
    print(f'median {ser.median()}')
    print(f'25th percentile {ser.quantile(0.25)}')
    print(f'75th percentile {ser.quantile(0.75)}')
    #or
    print(np.percentile(ser, q=[0,25,50,75,100]))

"""Calculte the frequency counts of each unique value ser"""
def ex9():
    ser = pd.Series(np.take(list('abcdefgh'), np.random.randint(8, size=30)))
    print(ser.value_counts())

"""From ser, keep the top 2 most frequent items as it is and replace everything else as ‘Other’."""
def ex10():
    np.random.RandomState(100)
    ser = pd.Series(np.random.randint(1, 5, [12]))
    print("before transformation")
    print(ser)
    print("occurrences")
    print(ser.value_counts())
    counts = ser.value_counts()
    ser[~ser.isin(ser.value_counts().index[:2])] = 'Other'
    print("after transformation")
    print(ser)

"""Bin the series ser into 10 equal deciles and replace the values with the bin name."""
def ex11():
    ser = pd.Series(np.random.random(20))
    labels = ['1st','2nd','3rd','4th','5th','6th','7th','8th','9th','10th']
    print(ser)
    print("equi height")
    print(pd.cut(ser, bins = 10, ordered=True, labels=labels))
    print("equi width")
    print(pd.qcut(ser, 10, labels=labels) == pd.qcut(ser, q=[0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1], labels=labels))

"""Reshape the series ser into a dataframe with 7 rows and 5 columns"""
def ex12():
    np_ar = pd.Series(np.random.randint(1, 10, 35)).values
    df = pd.DataFrame(np_ar.reshape([7,5]))
    print("final DATAFRAME")
    print(df)

"""Find the positions of numbers that are multiples of 3 from ser."""
def ex13():
    ser = pd.Series(np.random.randint(1, 10, 7))
    print(ser)
    print(f"multiples of 3 : \n{ser[ser % 3 == 0]}")
    print(f"indexes of multiples of 3 : \n{ser[ser % 3 == 0].index.values}")


"""From ser, extract the items at positions in list pos."""
def ex14():
    ser = pd.Series(list('abcdefghijklmnopqrstuvwxyz'))
    pos = [0, 4, 8, 14, 20]
    print(ser.iloc[pos])

"""Stack ser1 and ser2 vertically and horizontally (to form a dataframe)."""
def ex15():
    ser1 = pd.Series(range(5))
    ser2 = pd.Series(list('abcde'))
    print(pd.concat([ser1, ser2], ignore_index=True, axis=1))

"""Get the positions of items of ser2 in ser1 as a list."""
def ex16():
    ser1 = pd.Series([10, 9, 6, 5, 3, 1, 12, 8, 13])
    ser2 = pd.Series([1, 3, 10, 13])
    result = [key for (key, value) in ser1.isin(ser2).items() if value]
    print(result)
    #print([pd.Index(ser1).get_loc(i) for i in ser2])


"""Compute the mean squared error of truth and pred series.MSE"""
def ex17():
    truth = pd.Series(range(10))
    pred = pd.Series(range(10)) + np.random.random(10)
    result = sum((truth-pred)**2)/len(truth)
    print(result)
    # 2nd Solution
    print(np.mean((truth - pred) ** 2))


"""Change the first character of each word to upper case in each word of ser."""
def ex18():
    ser = pd.Series(['how', 'to', 'kick', 'ass?'])
    result = pd.Series([word[0].upper()+word[1:] for word in ser])

    # Solution 2
    result2 = ser.map(lambda x: x.title())
    # Solution 3
    result3 = ser.map(lambda x: x[0].upper() + x[1:])
    # Solution 4
    result4 = pd.Series([i.title() for i in ser])

    print(result)
    print(result2)
    print(result3)
    print(result4)

"""How to calculate the number of characters in each word in a series?"""
def ex19():
    ser = pd.Series(['how', 'to', 'kick', 'ass?'])
    result = ser.map(lambda word: len(word))
    print(result)


"""How to compute difference of differences between consequtive numbers of a series?"""
def ex20():
    ser = pd.Series([1, 3, 6, 10, 15, 21, 27, 35])
    #result = [ser[i+1] - ser[i] if i > 0 else None for i in (range(len(ser)-1))]

    # Solution
    print(ser.diff().tolist())
    print(ser.diff().diff().tolist())

"""How to convert a series of date-strings to a timeseries?"""
def ex21():
    ser = pd.Series(['01 Jan 2010', '02-02-2011', '20120303', '2013/04/04', '2014-05-05', '2015-06-06T12:20'])
    result = ser.astype('datetime64[ns]')
    ## Solution 2
    result2 = pd.to_datetime(ser)
    print(result)

"""How to get the day of month, week number, day of year and day of week from a series of date strings?"""
def ex22():
    ser = pd.Series(['01 Jan 2010', '02-02-2011', '20120303', '2013/04/04', '2014-05-05', '2015-06-06T12:20'])
    datetime_series = pd.to_datetime(ser)
    # day of month
    print("Date: ", datetime_series.dt.day.tolist())

    # week number
    print("Week number: ", datetime_series.dt.weekofyear.tolist())

    # day of year
    print("Day number of year: ", datetime_series.dt.dayofyear.tolist())

    # day of week
    print("Day of week: ", datetime_series.dt.weekday_name.tolist())

"""From ser, extract words that contain atleast 2 vowels."""
def ex24():
    ser = pd.Series(['Apple', 'Orange', 'Plan', 'Python', 'Money'])
    vowels = ['a','e','i','o','u']
    check_vowels = lambda w : sum([1 if letter in vowels else 0 for letter in list(w)])
    result = pd.Series([word for word in ser if check_vowels(word.lower()) > 1 ])
    print(result)

    #Solution 2
    from collections import Counter
    mask = ser.map(lambda x: sum([Counter(x.lower()).get(i, 0) for i in list('aeiou')]) >= 2)
    result2 = ser[mask]

"""Extract the valid emails from the series emails. The regex pattern for valid emails is provided as reference."""
def ex25():
    import regex as re
    emails = pd.Series(['buying books at amazom.com', 'rameses@egypt.com', 'matt@t.co', 'narendra@modi.com'])
    pattern = '[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}'
    mask = emails.map(lambda x: bool(re.match(pattern, x)))
    result = emails[mask]
    print(result)

"""Compute the mean of weights of each fruit."""
def ex26():
    fruit = pd.Series(np.random.choice(['apple', 'banana', 'carrot'], 10))
    weights = pd.Series(np.linspace(1, 10, 10))
    print(weights.tolist())
    print(fruit.tolist())
    concat = pd.concat([fruit, weights], axis=1)
    print(concat)
    print(concat.groupby([0]).mean())


"""Compute the euclidean distance between series (points) p and q, without using a packaged formula."""
def ex27():
    p = pd.Series([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    q = pd.Series([10, 9, 8, 7, 6, 5, 4, 3, 2, 1])
    euclidean_distance = math.sqrt(sum((p-q)**2))
    print(euclidean_distance)


"""Get the positions of peaks (values surrounded by smaller values on both sides) in ser."""
def ex28():
    ser = pd.Series([2, 10, 3, 4, 9, 10, 2, 7, 3])
    result = [i for i in range(1,len(ser)-1) if (ser[i] > ser[i-1] and ser[i] > ser[i+1])]
    print(result)

"""Replace the spaces in my_str with the least frequent character."""
def ex29():
    my_str = 'dbc deb abed gade'
    ser = pd.Series(list(my_str))
    freq = ser.value_counts()
    print("Freq",freq)
    least_freq = freq.dropna().index[-1]
    result = ser.replace(' ', least_freq)
    print(result)

"""How to import only every nth row from a csv file to create a dataframe?"""
def ex33():
    df = pd.read_csv("https://raw.githubusercontent.com/selva86/datasets/master/BostonHousing.csv", nrows=50)
    print(df.info())

"""Import the boston housing dataset, but while importing change the 'medv' (median house value) column so that values < 25 becomes ‘Low’ and > 25 becomes ‘High’."""
def ex34():
    df = pd.read_csv("https://raw.githubusercontent.com/selva86/datasets/master/BostonHousing.csv", nrows=50)
    # Solution 1: Using converter parameter
    df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/BostonHousing.csv',
                     converters={'medv': lambda x: 'High' if float(x) > 25 else 'Low'})

"""How to create a dataframe with rows as strides from a given series?"""
def ex35():
    L = pd.Series(range(15))
    print([L[i:i+4].tolist() for i in range(0,len(L),2)])

"""Import ‘crim’ and ‘medv’ columns of the BostonHousing dataset as a dataframe."""
def ex36():
    df = pd.read_csv("https://raw.githubusercontent.com/selva86/datasets/master/BostonHousing.csv",
                     nrows=50,
                     usecols=['crim','medv'])
    print(df)


"""How to get the nrows, ncolumns, datatype, summary stats of each column of a dataframe? Also get the array and list equivalent."""
def ex37():
    df = pd.read_csv("https://raw.githubusercontent.com/selva86/datasets/master/BostonHousing.csv",
                     nrows=10)
    print("Rows and columns: \n", df.shape)
    print("Data types: \n", df.dtypes)
    print("Summary statistics: ", df.describe())
    print("Numpy array: ", df.values)
    print("List: ", df.values.tolist())

"""How to extract the row and column number of a particular cell with given criterion?"""
def ex38():
    df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')
    result1 = df.loc[df.Price == df.Price.max()]
    print("Solution 1: \n", result1)
    row, col = np.where(df.values == np.max(df.Price))
    print(f'Solution 2: \n [ROW] {row} [COL] {col}')

"""Rename the column Type as CarType in df and replace the ‘.’ in column names with ‘_’."""
def ex39():
    df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')
    df = df.rename(columns={'Type':'CarType'})
    print(df.columns)
    df.columns = df.columns.map(lambda x : x.replace('.','-'))
    print(df.columns)

"""Check if df has any missing values"""
def ex40():
    df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')
    print(df.isnull().values.any())


"""Count the number of missing values in each column of df. Which column has the maximum number of missing values?"""
def ex41():
    df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')
    missing = np.sum(df.isna())
    result = missing[missing == missing.max()]
    print(result)

"""Replace missing values in Min.Price and Max.Price columns with their respective mean."""
def ex42():
    df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')
    print(df[['Min.Price','Max.Price']].head())
    df['Min.Price'] = df['Min.Price'].fillna(df['Min.Price'].mean())
    df['Max.Price'] = df['Max.Price'].fillna(df['Max.Price'].mean())
    print(df[['Min.Price','Max.Price']].head())

"""In df, use apply method to replace the missing values in Min.Price with the column’s mean and those in Max.Price with the column’s median."""
def ex43():
    df1 = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')
    df2 = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')

    #Solution 1
    def replace_with(x, value):
        if pd.isna(x) or pd.isnull(x):
            return value
        else:
            return x

    min_price_mean = df1['Min.Price'].mean()
    max_price_median = df1['Max.Price'].median()
    df1['Min.Price'] = df1['Min.Price'].apply(replace_with, args=(min_price_mean,))
    df1['Max.Price'] = df1['Max.Price'].apply(replace_with, args=(max_price_median,))

    #Solution 2
    # Solution
    d = {'Min.Price': np.nanmean, 'Max.Price': np.nanmedian}
    df2[['Min.Price', 'Max.Price']] = df2[['Min.Price', 'Max.Price']].apply(lambda x, d: x.fillna(d[x.name](x)),
                                                                          args=(d,))

    print(df1[['Min.Price', 'Max.Price']].head(10))
    print(df2[['Min.Price', 'Max.Price']].head(10))

"""Get the first column (a) in df as a dataframe (rather than as a Series)."""
def ex44():
    df = pd.DataFrame(np.arange(20).reshape(-1, 5), columns=list('abcde'))
    #Solution
    print(type(df[['a']]))
    print(df[['a']])
    print(type(df.iloc[:,[0]]))
    print(df.iloc[:,[0]])

""" - In df, interchange columns 'a' and 'c'.
    - Create a generic function to interchange two columns, without hardcoding column names."""
def ex45():
    df = pd.DataFrame(np.arange(20).reshape(-1, 5), columns=list('abcde'))
    print(df)
    # Solution Q1
    print(df[list('cbade')])
    # Solution Q2
    def switch_columns(df, col1=None, col2=None):
        colnames = df.columns.tolist()
        i1, i2 = colnames.index(col1), colnames.index(col2)
        colnames[i2], colnames[i1] = colnames[i1], colnames[i2]
        return df[colnames]
    switch_columns(df, 'a','c')

"""Change the pamdas display settings on printing the dataframe df it shows a maximum of 10 rows and 10 columns."""
def ex46():
    df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')
    print(df)
    print(df.iloc[:10,:10])


"""From df, filter the 'Manufacturer', 'Model' and 'Type' for every 20th row starting from 1st (row 0)."""
def ex49():
    df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv')
    print(df[['Manufacturer','Model','Type']][::20])


"""In df, Replace NaNs with ‘missing’ in columns 'Manufacturer', 'Model' and 'Type' and create a index as a combination of these three columns and check if the index is a primary key."""
def ex50():
    df = pd.read_csv('https://raw.githubusercontent.com/selva86/datasets/master/Cars93_miss.csv',
                     usecols=[0, 1, 2, 3, 5])
    df[['Manufacturer', 'Model','Type']] = df[['Manufacturer', 'Model','Type']].fillna(value='missing')
    df.set_index(['Manufacturer', 'Model','Type'])
    print(df)


"""Find the row position of the 5th largest value of column 'a' in df."""
def ex51():
    df = pd.DataFrame(np.random.randint(1, 30, 30).reshape(10, -1), columns=list('abc'))
    print(df)

    #Solution 1
    #get row with maximum value for 'a' column
    print(df[df['a'] == df['a'].max()])
    # get first 5th largest value
    print(df['a'].nlargest(5))
    print(df['a'].nlargest(5)[-1:])

    #Solution 2
    df['a'].argsort()[::-1][5]

"""How to find the position of the nth largest value greater than a given value?"""
def ex52():
    ser = pd.Series(np.random.randint(1, 100, 15))
    # todo


"""Get the last two rows of df whose row sum is greater than 100."""
def ex53():
    df = pd.DataFrame(np.random.randint(10, 40, 60).reshape(-1, 4))
    print(df)
    # print row sums
    rowsums = df.apply(np.sum, axis=1)
    print(df.iloc[np.where(rowsums > 100)][:][-2:])

"""Replace all values of ser in the lower 5%ile and greater than 95%ile with respective 5th and 95th %ile value."""
def ex54():
    ser = pd.Series(np.logspace(-2, 2, 30))
    low, high = ser.quantile([0.05,0.95])
    print("serie", ser)
    print("%_iles 0.05", low)
    print("%_iles 0.95", high)
    ser[ser < low] = low
    ser[ser > high] = high
    print(ser)


"""Swap rows 1 and 2 in df."""
def ex56():
    df = pd.DataFrame(np.arange(25).reshape(5, -1))
    print(df)
    print("SWAP ROW 1 AND ROW 2")
    df.iloc[[2,1][:]] = df.iloc[[1,2][:]]
    print(df)


"""Reverse all the rows of dataframe df."""
def ex57():
    df = pd.DataFrame(np.arange(25).reshape(5, -1))
    print(df)
    print("REVERSE")
    print(df.iloc[::-1])

"""Get one-hot encodings for column 'a' in the dataframe df and append it as columns."""
def ex58():
    df = pd.DataFrame(np.arange(25).reshape(5, -1), columns=list('abcde'))
    print(df)
    one_hot_a = pd.get_dummies(df['a'])
    print(pd.concat([one_hot_a, df[list('bcde')]], axis=1))


"""Obtain the column name with the highest number of row-wise maximum’s in df."""
def ex59():
    df = pd.DataFrame(np.random.randint(1, 100, 40).reshape(10, -1))
    print(df)
    column_with_max_per_row = df.apply(np.argmax, axis=1)
    # count how many maxs are for each columns
    print(column_with_max_per_row.value_counts())


"""Create a new column such that, each row contains the row number of nearest row-record by euclidean distance."""
def ex60():
    df = pd.DataFrame(np.random.randint(1, 100, 40).reshape(10, -1), columns=list('pqrs'), index=list('abcdefghij'))
    print(df)

    #Solution 1
    def euclidean_distance(a, b):
        return math.sqrt(np.sum((a-b)**2))

    distances = []
    indexes = []

    for i, row in df.iterrows():
        curr_row_ser = df.loc[[i]]
        rest_df = df.drop(i)
        rest_df['distance'] = rest_df.apply(lambda x: euclidean_distance(x, row), axis=1)
        index = rest_df[rest_df['distance'] == rest_df['distance'].min()].index
        nearest = np.min(rest_df['distance'])
        distances.append(nearest)
        indexes.append(index.values[0])

    df['distance'] = distances
    df['nearest-row'] = indexes
    print(df)

"""Compute maximum possible absolute correlation value of each column against other columns in df"""
def ex61():
    df = pd.DataFrame(np.random.randint(1, 100, 80).reshape(8, -1), columns=list('pqrstuvwxy'), index=list('abcdefgh'))
    print(df)

    # Solution 1
    corr_df = np.abs(df.corr())
    print(corr_df)
    np.fill_diagonal(corr_df.values, -999999999999)
    max_corr_list = list(corr_df.max(axis=0))
    print(corr_df.max(axis=0))

    # Solution 2
    abs_corrmat = np.abs(df.corr())
    max_corr = abs_corrmat.apply(lambda x: sorted(x)[-2])
    print('Maximum Correlation possible for each column: ', np.round(max_corr.tolist(), 2))


"""Compute the minimum-by-maximum for every row of df."""
def ex62():
    df = pd.DataFrame(np.random.randint(1, 100, 80).reshape(8, -1))
    min_by_max = df.apply(lambda x: np.min(x) / np.max(x), axis=1)


"""Create a new column 'penultimate' which has the second largest value of each row of df."""
def ex63():
    df = pd.DataFrame(np.random.randint(1, 100, 80).reshape(8, -1))
    print(df)
    df['penultimate'] = df.apply(lambda x: sorted(x)[-2], axis = 1)
    print("AFTER CHANGING")
    print(df)


"""
- Normalize all columns of df by subtracting the column mean and divide by standard deviation.
- Range all columns of df such that the minimum value in each column is 0 and max is 1.
"""
def ex64():
    df = pd.DataFrame(np.random.randint(1, 100, 80).reshape(8, -1))
    print(df)
    std = df.std(axis=0)
    print("Std dev.", std)
    mean = df.mean(axis=0)
    print("mean", mean)
    df = df.apply(lambda x: (x - mean)/std , axis = 1)
    print("after normalization")
    print(df)
    min_max_normalized = df.apply(lambda x: (x - min(x)) / (max(x)-min(x)), axis = 1)
    print(min_max_normalized)

"""Compute the correlation of each row of df with its succeeding row."""
def ex65():
    df = pd.DataFrame(np.random.randint(1, 100, 80).reshape(8, -1))
    corr = []
    for i in range(len(df)-1):
        corr.append(df.iloc[i].corr(df.iloc[i+1]))
    print(corr)


"""Replace both values in both diagonals of df with 0."""
def ex66():
    df = pd.DataFrame(np.random.randint(1, 100, 100).reshape(10, -1))
    print(df)
    np.fill_diagonal(df.values, 0)
    np.fill_diagonal(np.fliplr(df.values), 0)
    print(df)

"""From df_grouped, get the group belonging to 'apple' as a dataframe """
def ex67():
    df = pd.DataFrame({'col1': ['apple', 'banana', 'orange'] * 3,
                       'col2': np.random.rand(9),
                       'col3': np.random.randint(0, 15, 9)})

    df_grouped = df.groupby(['col1'])
    print(df_grouped.get_group('apple'))

"""In df, find the second largest value of 'taste' for 'banana'"""
def ex68():
    df = pd.DataFrame({'fruit': ['apple', 'banana', 'orange'] * 3,
                       'taste': np.random.rand(9),
                       'price': np.random.randint(0, 15, 9)})
    print(df)
    result = df.loc[df.fruit == 'banana'].sort_values(['taste'])
    print(result.iloc[-2])

"""In df, Compute the mean price of every fruit, while keeping the fruit as another column instead of an index."""
def ex69():
    df = pd.DataFrame({'fruit': ['apple', 'banana', 'orange'] * 3,
                       'rating': np.random.rand(9),
                       'price': np.random.randint(0, 15, 9)})
    print(df)
    result = df.groupby(as_index=False, by='fruit')['price'].mean()
    print(result)


"""Join dataframes df1 and df2 by ‘fruit-pazham’ and ‘weight-kilo’."""
def ex70():
    df1 = pd.DataFrame({'fruit': ['apple', 'banana', 'orange'] * 3,
                        'weight': ['high', 'medium', 'low'] * 3,
                        'price': np.random.randint(0, 15, 9)})
    print(df1)

    df2 = pd.DataFrame({'pazham': ['apple', 'orange', 'pine'] * 2,
                        'kilo': ['high', 'low'] * 3,
                        'price': np.random.randint(0, 15, 6)})
    print(df2)

    print(df1.merge(df1, df2, how='inner', left_on=['fruit', 'weight'], rright_on=['pazham', 'pounds']))

"""From df1, remove the rows that are present in df2. All three columns must be the same."""
def ex71():
    df1 = pd.DataFrame({'fruit': ['apple', 'banana', 'orange'] * 3,
                        'weight': ['high', 'medium', 'low'] * 3,
                        'price': np.random.randint(0, 15, 9)})

    df2 = pd.DataFrame({'pazham': ['apple', 'orange', 'pine'] * 2,
                        'kilo': ['high', 'low'] * 3,
                        'price': np.random.randint(0, 15, 6)})
    print("DF1")
    print(df1)
    print("DF2")
    print(df2)
    print(df1.isin(df2))
    print(df1[-df1.isin(df2).all(1)])

"""How to get the positions where values of two columns match?"""
def ex72():
    df = pd.DataFrame({'fruit1': np.random.choice(['apple', 'orange', 'banana'], 10),
                       'fruit2': np.random.choice(['apple', 'orange', 'banana'], 10)})
    print(df)
    print(df.iloc[np.where(df.fruit1 == df.fruit2)])

"""Create two new columns in df, one of which is a lag1 (shift column a down by 1 row) of column ‘a’ and the other is a lead1 (shift column b up by 1 row)"""
def ex73():
    df = pd.DataFrame(np.random.randint(1, 100, 20).reshape(-1, 4), columns=list('abcd'))
    print(df)
    df['lag1'] = df['a'].shift(1)
    df['lead'] = df['b'].shift(-1)
    print("After shifting A")
    print(df)

"""Get the frequency of unique values in the entire dataframe df."""
def ex74():
    df = pd.DataFrame(np.random.randint(1, 10, 20).reshape(-1, 4), columns=list('abcd'))
    print(df)
    print("____")
    print(np.unique( df.values, return_counts=True))

    # Solution 2
    print(pd.value_counts(df.values.ravel()))

"""Split the string column in df to form a dataframe with 3 columns as shown."""
def ex75():
    df = pd.DataFrame(["STD, City    State",
                       "33, Kolkata    West Bengal",
                       "44, Chennai    Tamil Nadu",
                       "40, Hyderabad    Telengana",
                       "80, Bangalore    Karnataka"], columns=['row'])
    # Solution
    df_out = df.row.str.split(',|\t', expand=True)

    # Make first row as header
    new_header = df_out.iloc[0]
    df_out = df_out[1:]
    df_out.columns = new_header
    print(df_out)

if __name__ == "__main__":
    ex75()
