import pandas as pd
import numpy as np

# crete a sample dataframe
data = pd.DataFrame({
    'age' :     [ 10, 22, 13, 21, 12, 11, 17],
    'section' : [ 'A', 'B', 'C', 'B', 'B', 'A', 'A'],
    'city' :    [ 'Gurgaon', 'Delhi', 'Mumbai', 'Delhi', 'Mumbai', 'Delhi', 'Mumbai'],
    'gender' :  [ 'M', 'F', 'F', 'M', 'M', 'M', 'F'],
    'favourite_color' : [ 'red', np.NAN, 'yellow', np.NAN, 'black', 'green', 'red']
})

# view the data
print(data)

#select rows with a condition
print(data.loc[data.age >= 15])

#select rows with multiple conditions
print(data.loc[(data.age >= 12) & (data.gender == "M")])

#slice
print(data.loc[1:3])

#select a subset of columns with a condition
print(data.loc[(data.age >= 12),['age','gender']])

# update a column with condition
#  if the values in age are greater than equal to 12, then we want to update the values of the column section to be “M”.
data.loc[(data.age >= 12), ['section']] = 'M'
print(data)


# update multiple columns with condition
data.loc[(data.age >= 20), ['section', 'city']] = ['S','Pune']
data


########### ------- ILOC -------- ############
# select rows with indexes
print(data.iloc[[0,2]])

# select rows with particular indexes and particular columns
print(data.iloc[[0,2],[1,3]])

# select a range of rows and columns
print(data.iloc[1:3,2:4])



