Implement the MapSum class:

- <code> MapSum() </code> Initializes the MapSum object.
- <code>void insert(String key, int val) </code>Inserts the key-val pair into the map. If the key already existed, the original key-value pair will be overridden to the new one.
- <code>int sum(string prefix) </code>Returns the sum of all the pairs' value whose key starts with the prefix.

## Example
<code>
MapSum mapSum = new MapSum()
mapSum.insert("apple", 3) 
mapSum.sum("ap") // return 3 (apple = 3)
mapSum.insert("app", 2)    
mapSum.sum("ap")// return 5 (apple + app = 3 + 2 = 5)
</code>

## Constraints
- 1 <= key.length, prefix.length <= 50
- key and prefix consist of only lowercase English letters.
- 1 <= val <= 1000
- At most 50 calls will be made to insert and sum