class MapSum:
    def __init__(self, initial_map={}):
        self.map = initial_map

    def insert(self, key, value):
        self.map.update({key: value})
        """Or self.map[key] = value"""

    def sum(self, prefix):
        return sum([value if key.startswith(prefix) else 0 for (key, value) in self.map.items()])

if __name__ == "__main__":
    mapsum = MapSum()
    mapsum.insert("a",3)
    print(mapsum.sum("ap"))
    mapsum.insert("b",5)
    print(mapsum.sum("a"))

