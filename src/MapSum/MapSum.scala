package MapSum

import collection.mutable.Map

class MapSum(private var map : Map[String, Int] = Map()) {

  def insert(key: String, `val`: Int) {
    map += (key -> `val`)
  }

  def sum(prefix: String): Int = {
    map.filter(t => t._1.startsWith(prefix)).foldLeft(0)((acc, kv) => acc + kv._2)
  }

}

