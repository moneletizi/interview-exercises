import regex as re
class WordDictionary:

    def __init__(self, words = []):
        self.words = words

    def addWord(self, word: str) -> None:
        self.words.append(word)

    def search(self, word: str) -> bool:
        return sum([1 if self.__match_words(word_, word) else 0 for word_ in self.words]) > 0

    def __match_words(self, word_willcards, word):
        willcards_indices = [_.start() for _ in re.finditer(".", word_willcards)]
        for index in willcards_indices:
            word = word[:index] + '.' + word[index+1:]
        if len(word) != len(word_willcards):
            return False
        elif word == word_willcards:
            return True
        else:
            return False
