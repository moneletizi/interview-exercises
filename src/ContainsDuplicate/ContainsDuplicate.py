from typing import List

"""Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct."""
class Solution:
    def containsDuplicate(self, nums: List[int]) -> bool:
        return len(nums) != len(set(nums))

# better solution (beats 93% of submissions by memory usage and 63% by runtime)
class Solution2:
    def containsDuplicate(self, nums: List[int]) -> bool:
        nums.sort()
        i = 0
        for j in range(1, len(nums)):
            if nums[i] == nums[j]:
                return True
            i = i + 1
        return False



if __name__ == "__main__":
    sol = Solution2()
    assert sol.containsDuplicate([1,2,3,1])
    assert sol.containsDuplicate([1, 2, 3, 4]) == False
    assert sol.containsDuplicate([1,1,1,3,3,4,3,2,4,2])
