from typing import List


class Solution:
    def removeDuplicates(self, nums: List[int]) -> int:
        unique_elements_list = list(set(nums))
        unique_elements_list.sort()
        num_unique_elements = len(unique_elements_list)
        for i in range(num_unique_elements):
            nums[i] = unique_elements_list[i]
        for j in range(num_unique_elements, len(nums)):
            nums[j] = "-"
        return num_unique_elements

if __name__=="__main__":
    sol = Solution()
    assert sol.removeDuplicates([-1,0,0,0,0,3,3]) == 3
